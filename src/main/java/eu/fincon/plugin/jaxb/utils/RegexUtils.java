package eu.fincon.plugin.jaxb.utils;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Provides methods based on regular expressions.
 * Created : 26.02.2018
 *
 * @author : mjohannsen
 */
public class RegexUtils {

	private static final String NULL_IS_PROVIDED_TO_MEHTOD = "null is provided to mehtod";
	private static final String NAMESPACE_PATTERN = "\\(namespace = \"(http:\\/\\/[a-zA-Z0-9\\/\\.\\-]*)";
	private static final String REGEX_PATTERN_NON_GREEDY = ".*?";

	private RegexUtils() {
	}

	/**
	 * Search in a given value for namespace and returns the match.
	 *
	 * @param value given {@link String}
	 * @return the match of type {@link String}, otherwise null
	 */
	public static String getNamespace(String value) {

		Objects.requireNonNull(value, NULL_IS_PROVIDED_TO_MEHTOD);

		Pattern pattern = Pattern.compile(REGEX_PATTERN_NON_GREEDY + NAMESPACE_PATTERN, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
		Matcher matcher = pattern.matcher(value);
		if (matcher.find()) {
			return matcher.group(1);
		}
		return null;
	}
}
