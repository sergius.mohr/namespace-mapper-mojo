package eu.fincon.plugin.jaxb.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Loads the required mapping file into a map and provided a method to get value by key.
 * <p>
 * Created : 26.02.2018
 *
 * @author : mjohannsen
 */
public class MappingCache {

	private static final String LINE_SEPERATOR = "\\|";
	private static Logger logger = LoggerFactory.getLogger(MappingCache.class);

	private Map<String, String> keyValueMap = new HashMap<>();

	/**
	 * @param mappingFile
	 * 		file that contains the mapping
	 */
	public MappingCache(File mappingFile) {
		processFile(mappingFile);
	}

	private void processFile(File file) {
		try (Stream<String> stream = Files.lines(Paths.get(file.getAbsolutePath()))) {

			stream.forEach(line -> {
				String[] split = line.split(LINE_SEPERATOR);

				validateLine(split, line);

				addToCache(split);
			});

			logger.info("{} values added to mapCache", keyValueMap.size());

		} catch (IOException e) {
			// something went really wrong, parameter is required and already validated in NameSpaceMapperMojo class.
			logger.error("Can not process mapping file {}", file.getAbsolutePath());
			throw new IllegalArgumentException("Not a valid mapping file");
		}
	}

	private void addToCache(String[] split) {
		if (keyValueMap.containsKey(split[0])) {
			logger.info("cache contains already a value <{}>, ignoring", split[0]);
		}
		keyValueMap.put(split[1], split[0]);
	}

	private void validateLine(String[] split, String line) {
		if (split.length != 2) {
			logger.error("line <{}> must contains 2 elements", line);
			throw new ArrayIndexOutOfBoundsException("line <" + line + "> must contains 2 elememtns");
		}

	}

	/**
	 * Returns the value by a given key.
	 *
	 * @param key
	 * 		for lookup
	 *
	 * @return Value by key, otherwise null
	 */
	public String getValueByKey(String key) {
		return keyValueMap.getOrDefault(key, null);
	}


}
