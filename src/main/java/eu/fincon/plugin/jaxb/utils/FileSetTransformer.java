package eu.fincon.plugin.jaxb.utils;

import org.apache.maven.model.FileSet;
import org.codehaus.plexus.util.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Transforms a {@link FileSet} to a {@link List} of Files.
 * Created : 26.02.2018
 *
 * @author : mjohannsen
 */
public final class FileSetTransformer {

	private FileSetTransformer() {
	}

	/**
	 * Transform a given {@link FileSet} to a List of File elements.
	 *
	 * @param fileSet
	 * 		given FileSet
	 *
	 * @return List<File>
	 *
	 * @throws IOException
	 * 		can not get Files from FileSet
	 */
	public static List<File> toFileList(FileSet fileSet) throws IOException {
		File directory = new File(fileSet.getDirectory());
		String includes = toString(fileSet.getIncludes());
		String excludes = toString(fileSet.getExcludes());
		return FileUtils.getFiles(directory, includes, excludes);
	}

	private static String toString(List<String> strings) {
		StringBuilder sb = new StringBuilder();
		for (String string : strings) {
			if (sb.length() > 0)
				sb.append(", ");
			sb.append(string);
		}
		return sb.toString();
	}
}