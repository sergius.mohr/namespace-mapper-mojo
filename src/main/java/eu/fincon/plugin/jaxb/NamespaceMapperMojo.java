package eu.fincon.plugin.jaxb;

import eu.fincon.plugin.jaxb.utils.FileSetTransformer;
import eu.fincon.plugin.jaxb.utils.MappingCache;
import eu.fincon.plugin.jaxb.utils.RegexUtils;
import org.apache.maven.model.FileSet;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

import static java.lang.String.format;
import static org.apache.commons.io.FileUtils.readFileToString;
import static org.apache.commons.io.FileUtils.writeStringToFile;

/**
 * Goal which generates and adds an additional prefix to XmlSchema annotation in package-info.class.
 * Created : 26.02.2018
 *
 * @author : mjohannsen
 */
@Mojo(name = "namespaceMapper")
public class NamespaceMapperMojo extends AbstractMojo {

	private static final String XML_NS_ANNOTATION = "@javax.xml.bind.annotation.XmlNs(namespaceURI";
	private static final String PREFIX_NAMESPACE_ANNOTATION = ", xmlns = {\n    @javax.xml.bind.annotation.XmlNs(namespaceURI = \"%s\", prefix = \"%s\")\n})";
	private static final String UFT_8_ENCODING = "utf-8";
	private static Logger logger = LoggerFactory.getLogger(NamespaceMapperMojo.class);

	public void setProject(MavenProject project) {
		this.project = project;
	}

	@Parameter(defaultValue = "${project}", readonly = true, required = true)
	private MavenProject project;

	@Parameter(property = "namespaceMapper.mappingFile")
	private File mappingFile;

	@Parameter(defaultValue = "target/generated", property = "namespaceMapper.outputDirectory")
	private String outputDirectory;

	@Override
	public void execute() throws MojoExecutionException {

		validateMappingFile(mappingFile);

		MappingCache mappingCache = new MappingCache(mappingFile);

		String outDirectory = format("%s/%s", project.getBasedir().getAbsolutePath(), outputDirectory);

		logger.info("outDirectory: {}", outDirectory);

		final FileSet xmlFiles = new FileSet();

		configureFileSet(outDirectory, xmlFiles);

		try {
			List<File> files = FileSetTransformer.toFileList(xmlFiles);
			for (File packageInfoFile : files) {
				logger.info("processing {}", packageInfoFile.getAbsolutePath());

				String fileContent = readFileToString(packageInfoFile, UFT_8_ENCODING);
				fileContent = processFileContent(mappingCache, fileContent);

				writeStringToFile(packageInfoFile, fileContent, UFT_8_ENCODING);
			}
		} catch (IOException e) {
			throw new MojoExecutionException("Can not process file");
		}

	}

	private void configureFileSet(String outDirectory, FileSet xmlFiles) {
		xmlFiles.setDirectory(outDirectory);
		xmlFiles.addInclude("**/package-info.java");
	}

	protected String processFileContent(MappingCache mappingCache, String fileContent) {
		if (fileContent.contains(XML_NS_ANNOTATION)) {
			return fileContent;
		}

		String namespace = RegexUtils.getNamespace(fileContent);
		String valueByKey = mappingCache.getValueByKey(namespace);

		if (null != valueByKey) {
			logger.info("Add XmlNs annoation: prefix <{}> for namespace <{}>", valueByKey, namespace);
			fileContent = fileContent.replace(")", String.format(PREFIX_NAMESPACE_ANNOTATION, namespace, valueByKey));
		} else {
			logger.info("namespace <{}> not found in mappingCache", namespace);
		}

		return fileContent;
	}

	private void validateMappingFile(File file) {
		Objects.requireNonNull(file, "mapping file not set");

		if (!file.exists()) {
			throw new IllegalArgumentException("mapping file does not exist");
		}
	}
}
