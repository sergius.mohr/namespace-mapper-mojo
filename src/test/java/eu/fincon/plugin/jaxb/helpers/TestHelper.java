package eu.fincon.plugin.jaxb.helpers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class TestHelper {

	private TestHelper() {
	}

	/**
	 * Converts an {@link InputStream} into a {@link String}.
	 *
	 * @param input of type {@link InputStream}
	 * @return converted {@link InputStream}
	 * @throws IOException can not load InputStream
	 */
	public static String read(InputStream input) throws IOException {
		try (BufferedReader buffer = new BufferedReader(new InputStreamReader(input))) {
			return buffer.lines().collect(Collectors.joining("\n"));
		}
	}

}
