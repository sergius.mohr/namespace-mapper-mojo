package eu.fincon.plugin.jaxb;

import eu.fincon.plugin.jaxb.helpers.TestHelper;
import eu.fincon.plugin.jaxb.utils.MappingCache;
import org.assertj.core.api.Assertions;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class NamespaceMapperMojoTest {

	private static final String FILE_WITH_PREFIX = "new-package-info.txt";
	private static final String ORIGINAL_PACKAGE_INFO = "original_package-info.txt";
	private static final String PACKAGE_INFO_ERROR_TXT = "package-info-error.txt";

	@DataProvider(name = "dataProvider")
	public Object[][] dataProvider() {
		return new Object[][]{
				{"findAndReplace", ORIGINAL_PACKAGE_INFO, FILE_WITH_PREFIX},
				{"alreadyAdded", FILE_WITH_PREFIX, FILE_WITH_PREFIX},
				{"wrongXmlSchema", PACKAGE_INFO_ERROR_TXT, PACKAGE_INFO_ERROR_TXT}
		};
	}


	@SuppressWarnings("squid:S00100")
	@Test(dataProvider = "dataProvider")
	public void test_ProcessFileContent_not_add(String testCase, String filenamePackageInfo, String fileExpectedResult) throws IOException {
		String packageInfoContent = TestHelper.read(getClass().getClassLoader().getResourceAsStream(filenamePackageInfo));
		String expectedPackageInfoContent = TestHelper.read(getClass().getClassLoader().getResourceAsStream(fileExpectedResult));

		File mappingFile = loadFile("mapping_duplicate_entry.csv");
		MappingCache mappingCache = new MappingCache(mappingFile);
		NamespaceMapperMojo mojo = new NamespaceMapperMojo();
		String actualPackageInfoContent = mojo.processFileContent(mappingCache, packageInfoContent);
		Assertions.assertThat(actualPackageInfoContent).isEqualTo(expectedPackageInfoContent);
	}

	private File loadFile(String filename) {
		URL url = getClass().getClassLoader().getResource(filename);

		if (null == url) {
			throw new NullPointerException("Can not load file from resources");
		}

		return new File(url.getFile());
	}
}