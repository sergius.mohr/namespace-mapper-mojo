# Namespace-Mapper-Mojo

## Usage

````xml
	<build>
		<plugins>
            <plugin>
                <groupId>eu.fincon.mojo</groupId>
                <artifactId>namespace-mapper-plugin</artifactId>
                <version>1.0.0</version>
                <configuration>
                    <mappingFile>src/main/resources/mapping.csv</mappingFile>
                </configuration>
                <executions>
                    <execution>
                        <id>generate-sources</id>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>namespaceMapper</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
		</plugins>
	</build>
````

